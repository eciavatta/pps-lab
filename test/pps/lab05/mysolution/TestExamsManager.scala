package pps.lab05.mysolution

import org.scalatest.Matchers._
import org.scalatest._
import pps.lab05.mysolution.ExamsManagerTest._
import pps.lab05.mysolution.TestExamsManager._

class TestExamsManager extends FlatSpec with ExamResultsBasicBehavior with ExamManagerBasicBehaviour

object TestExamsManager {

  trait ExamResultsBasicBehavior extends FlatSpec {

    import ExamResult._

    "Failed exam" should "have no vote" in {
      failed.valuation shouldBe empty
      failed.cumLaude shouldBe false
      failed toString() shouldBe "FAILED"
    }

    "Retired student" should "have no vote" in {
      retired.valuation shouldBe empty
      retired.cumLaude shouldBe false
      retired toString() shouldEqual "RETIRED"
    }

    "With laude vote" should "be succeeded" in {
      succeededCumLaude.valuation shouldBe Some(30)
      succeededCumLaude.cumLaude shouldBe true
      succeededCumLaude toString() shouldBe "SUCCEEDED(30L)"
    }

    "Exam passed" should "not have laude" in {
      succeeded(28).valuation shouldBe Some(28)
      succeeded(28).cumLaude shouldBe false
      succeeded(28) toString() shouldBe "SUCCEEDED(28)"
    }

    "Evaluation" should "be not greater than 30" in {
      assertThrows[IllegalArgumentException] {
        succeeded(32)
      }
    }

    it should "be not smaller than 18" in {
      assertThrows[IllegalArgumentException] {
        succeeded(17)
      }
    }

  }

  trait ExamManagerBasicBehaviour extends FlatSpec with BeforeAndAfterEach {

    var em: ExamsManager = _

    override def beforeEach(): Unit = {
      em = ExamsManager.apply

      em.createNewCall("January")
      em.createNewCall("February")
      em.createNewCall("March")

      import ExamResult._

      em.addStudentResult("January", "rossi", failed)
      em.addStudentResult("January", "bianchi", retired)
      em.addStudentResult("January", "verdi", succeeded(28))
      em.addStudentResult("January", "neri", succeededCumLaude)

      em.addStudentResult("February", "rossi", failed)
      em.addStudentResult("February", "bianchi", succeeded(20))
      em.addStudentResult("February", "verdi", succeeded(30))

      em.addStudentResult("March", "rossi", succeeded(25))
      em.addStudentResult("March", "bianchi", succeeded(25))
      em.addStudentResult("March", "viola", failed)
    }

    "ExamsManager" should "retrieve participants from the January and March appeals" in {
      em.getAllStudentsFromCall("January") shouldBe Set("rossi", "bianchi", "verdi", "neri")
      em.getAllStudentsFromCall("March") shouldBe Set("rossi", "bianchi", "viola")
    }

    it should "retrieve the participants of January promoted with vote" in {
      em.getEvaluationsMapFromCall("January").size shouldBe 2
      em.getEvaluationsMapFromCall("January")("verdi") shouldBe 28
      em.getEvaluationsMapFromCall("January")("neri") shouldBe 30
    }

    it should "retrieve the participants of February promoted with vote" in {
      em.getEvaluationsMapFromCall("February").size shouldBe 2
      em.getEvaluationsMapFromCall("February")("bianchi") shouldBe 20
      em.getEvaluationsMapFromCall("February")("verdi") shouldBe 30
    }

    it should "retrieve the results of all student Rossi's appeals" in {
      em.getResultsMapFromStudent("rossi").size shouldBe 3
      em.getResultsMapFromStudent("rossi")("January") shouldBe "FAILED"
      em.getResultsMapFromStudent("rossi")("February") shouldBe "FAILED"
      em.getResultsMapFromStudent("rossi")("March") shouldBe "SUCCEEDED(25)"
    }

    it should "retrieve the results of all student Bianchi's appeals" in {
      em.getResultsMapFromStudent("bianchi").size shouldBe 3
      em.getResultsMapFromStudent("bianchi")("January") shouldBe "RETIRED"
      em.getResultsMapFromStudent("bianchi")("February") shouldBe "SUCCEEDED(20)"
      em.getResultsMapFromStudent("bianchi")("March") shouldBe "SUCCEEDED(25)"
    }

    it should "retrieve the results of all student Neri's appeals" in {
      em.getResultsMapFromStudent("neri").size shouldBe 1
      em.getResultsMapFromStudent("neri")("January") shouldBe "SUCCEEDED(30L)"
    }

    it should "retrieve the best result from a student" in {
      em.getBestResultFromStudent("rossi") shouldBe Some(25)
      em.getBestResultFromStudent("bianchi") shouldBe Some(25)
      em.getBestResultFromStudent("neri") shouldBe Some(30)
      em.getBestResultFromStudent("viola") shouldBe None
    }

    it should "not permit to create two identical calls" in {
      assertThrows[IllegalArgumentException] {
        em.createNewCall("March")
      }
    }

    it should "not permit to register two identical evaluations" in {
      assertThrows[IllegalArgumentException] {
        em.addStudentResult("January", "verdi", ExamResult.failed)
      }
    }

  }

}
