package pps.lab02;

import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public abstract class SequenceGeneratorTest {

    private static final int ELEMENTS_COUNT = 10;

    @Test
    public void shouldNotBeOverAtStart() {
        assertFalse(getGenerator().isOver());
    }

    @Test
    public void shouldReturnCorrectNumberOfElements() {
        for (int i = 0; i < ELEMENTS_COUNT; i++) {
            assertTrue(getGenerator().next().isPresent());
        }

        assertFalse(getGenerator().next().isPresent());
    }

    protected abstract SequenceGenerator getGenerator();

    protected List<Optional<Integer>> consumeElements(int count) {
        return IntStream.range(0, count).mapToObj(i -> getGenerator().next()).collect(Collectors.toList());
    }

}