package pps.lab02;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class RangeGeneratorTest extends SequenceGeneratorTest {

    private static final int RANGE_START = 1;
    private static final int RANGE_STOP = 10;
    private static final int RANGE_MIDDLE = 5;

    private RangeGenerator generator;

    @Before
    public void setUp() {
        generator = new RangeGenerator(RANGE_START, RANGE_STOP);
    }

    @Test
    public void shouldReturnCorrectElements() {
        for (int i = RANGE_START; i < RANGE_STOP; i++) {
            Optional<Integer> value = getGenerator().next();
            assertTrue(value.isPresent());
            assertEquals(i, (int) value.get());
        }
    }

    @Test
    public void shouldReturnEmptyWhenIsOver() {
        consumeElements(RANGE_STOP);
        assertFalse(getGenerator().next().isPresent());
        assertTrue(getGenerator().isOver());
    }

    @Test
    public void shouldBeAtInitialStateAfterReset() {
        consumeElements(RANGE_MIDDLE);
        getGenerator().reset();
        assertEquals(RANGE_START, getGenerator().next().get().intValue());
    }

    @Test
    public void shouldReturnNoRemainingIfIsOver() {
        consumeElements(RANGE_STOP);
        assertTrue(getGenerator().allRemaining().isEmpty());
    }

    @Override
    protected SequenceGenerator getGenerator() {
        return generator;
    }
}