package pps.lab02;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RandomGeneratorTest extends SequenceGeneratorTest {

    private static final int BIT_NUMBERS = 10;

    private RandomGenerator generator;

    @Before
    public void setUp() {
        generator = new RandomGenerator(BIT_NUMBERS);
    }

    @Test
    public void shouldReturnSameSequenceAfterReset() {
        List<Optional<Integer>> first = consumeElements(BIT_NUMBERS);
        getGenerator().reset();
        assertEquals(first, consumeElements(BIT_NUMBERS));
    }

    @Override
    protected SequenceGenerator getGenerator() {
        return generator;
    }
}