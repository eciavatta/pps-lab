package pps.lab02;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private final List<Integer> randoms;
    private int index;

    public RandomGenerator(int n) {
        randoms = IntStream.range(0, n).map(i -> (Math.random() > 0.5) ? 1 : 0).boxed().collect(Collectors.toList());
        index = 0;
    }

    @Override
    public Optional<Integer> next() {
        if (isOver()) {
            return Optional.empty();
        }

        return Optional.of(randoms.get(index++));
    }

    @Override
    public void reset() {
        index = 0;
    }

    @Override
    public boolean isOver() {
        return !(index < randoms.size());
    }

    @Override
    public List<Integer> allRemaining() {
        return null;
    }
}
