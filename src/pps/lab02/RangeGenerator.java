package pps.lab02;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private final int start;
    private final int stop;
    private int current;

    public RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
        this.current = start;
    }

    @Override
    public Optional<Integer> next() {
        if (isOver()) {
            return Optional.empty();
        }

        return Optional.of(current++);
    }

    @Override
    public void reset() {
        current = start;
    }

    @Override
    public boolean isOver() {
        return !(current <= stop);
    }

    @Override
    public List<Integer> allRemaining() {
        return IntStream.rangeClosed(current, stop).boxed().collect(Collectors.toList());
    }
}
