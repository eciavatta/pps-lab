package pps.lab06.mysolution

import scala.language.postfixOps

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board collectFirst {
    case Mark(`x`, `y`, p) => p
  }

  def placeAnyMark(board: Board, player: Player): Seq[Board] =
    for (x <- 0 to 2; y <- 0 to 2; if find(board, x, y) isEmpty) yield board :+ Mark(x, y, player)

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = {
    def _computeGames(board: Board, player: Player, moves: Int): Stream[Game] = moves match {
      case n if winner(board).isDefined || n <= 0 => Stream(List(board))
      case _  => for (comb <- placeAnyMark(board, player.other).toStream;
                              path <- _computeGames(comb, player.other, moves - 1)
                             ) yield path :+ board
    }

    _computeGames(List.empty, player, moves)
  }

  def winner(board: Board): Option[Player] =
    if (checkWinning(board, O)) Some(O) else if (checkWinning(board, X)) Some(X) else None

  def checkWinning(board: Board, player: Player): Boolean =
    checkDiagonally(board, player) || checkVertically(board, player) || checkHorizontally(board, player)

  def checkDiagonally(board: Board, player: Player): Boolean =
    (0 to 2).forall(d => board.contains(Mark(d, d, player))) || (0 to 2).forall(d => board.contains(Mark(2 - d, d, player)))

  def checkVertically(board: Board, player: Player): Boolean =
    (0 to 2).exists(x => (0 to 2).forall(y => board.contains(Mark(x, y, player))))

  def checkHorizontally(board: Board, player: Player): Boolean =
    (0 to 2).exists(y => (0 to 2).forall(x => board.contains(Mark(x, y, player))))


  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...

  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 6) foreach {g => printBoards(g); println()}

  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}