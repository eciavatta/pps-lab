package pps.lab05.mysolution

import scala.language.postfixOps

object ExamsManagerTest extends App {

  trait ExamsManager {
    def createNewCall(call: String): Unit

    def addStudentResult(call: String, student: String, result: ExamResult): Unit

    def getAllStudentsFromCall(call: String): Set[String]

    def getEvaluationsMapFromCall(call: String): Map[String, Int]

    def getResultsMapFromStudent(student: String): Map[String, String]

    def getBestResultFromStudent(student: String): Option[Int]
  }

  object ExamsManager {
    def apply: ExamsManager = new ExamsManagerImpl()

    private class ExamsManagerImpl extends ExamsManager {
      private var calls: Map[String, Map[String, ExamResult]] = Map.empty

      override def createNewCall(call: String): Unit = {
        require(!(calls contains call))
        calls = calls + (call -> Map.empty)
      }

      override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
        require(!(calls(call) contains student))
        calls = calls + (call -> (calls(call) + (student -> result)))
      }

      override def getAllStudentsFromCall(call: String): Set[String] = calls(call).keySet

      override def getEvaluationsMapFromCall(call: String): Map[String, Int] = calls(call) collect {
        case (student, ExamSucceeded(evaluation, _)) => student -> evaluation
      }

      override def getResultsMapFromStudent(student: String): Map[String, String] = calls collect {
        case (call, map) if map contains student => call -> map(student).toString
      }

      override def getBestResultFromStudent(student: String): Option[Int] = calls collect {
        case (_, map) if map contains student => map(student).valuation
      } max
    }

  }

  sealed trait ExamResult {
    def valuation: Option[Int] = None

    def cumLaude: Boolean = false
  }

  case object ExamFailed extends ExamResult {
    override val toString: String = "FAILED"
  }

  case object ExamRetired extends ExamResult {
    override val toString: String = "RETIRED"
  }

  case class ExamSucceeded(private val evaluation: Int, override val cumLaude: Boolean) extends ExamResult {
    override val valuation: Option[Int] = Some(evaluation)

    override val toString: String = s"SUCCEEDED($evaluation${if (cumLaude) "L" else ""})"
  }

  object ExamResult {
    val failed: ExamResult = ExamFailed
    val retired: ExamResult = ExamRetired
    val succeededCumLaude: ExamResult = ExamSucceeded(30, cumLaude = true)

    def succeeded(evaluation: Int): ExamResult = {
      require(evaluation >= 18 && evaluation <= 30)
      ExamSucceeded(evaluation, cumLaude = false)
    }
  }

}