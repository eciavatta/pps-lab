package pps.lab03

sealed trait List[E]

object List {

  case class Cons[E](head: E, tail: List[E]) extends List[E]

  case class Nil[E]() extends List[E]

  def sum(l: List[Int]): Int = l match {
    case Cons(h, t) => h + sum(t)
    case _ => 0
  }

  def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
    case (Cons(h, t), _l2) => Cons(h, append(t, _l2))
    case (_l1, Cons(h, t)) => Cons(h, append(_l1, t))
    case _ => Nil()
  }

  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (Cons(_, t), _n) if n > 0 => drop(t, _n - 1)
    case _ => l
  }

  def map[A, B](l: List[A])(f: A => B): List[B] = l match {
    case Cons(h, t) => Cons(f(h), map(t)(f))
    case _ => Nil()
  }

  def filter[A](l: List[A])(p: A => Boolean): List[A] = l match {
    case Cons(h, t) if p(h) => Cons(h, filter(t)(p))
    case Cons(_, t) => filter(t)(p)
    case _ => Nil()
  }

  def max(l: List[Int]): Option[Int] = {
    def _max(l: List[Int], n: Option[Int]): Option[Int] = (l, n) match {
      case (Cons(h, t), Some(_n)) if h > _n => _max(t, Some(h))
      case (Cons(h, t), Some(_n)) if h <= _n => _max(t, Some(_n))
      case (Cons(h, t), None) => _max(t, Some(h))
      case (Nil(), Some(_n)) => Some(_n)
      case _ => None
    }

    _max(l, None)
  }

  def foldLeft[A](l: List[A])(acc: A)(red: (A, A) => A): A = l match {
    case Cons(h, t) => foldLeft(t)(red(acc, h))(red)
    case Nil() => acc
  }

  def foldRight[A](l: List[A])(acc: A)(f: (A, A) => A): A = l match {
    case Cons(h, t) => f(h, foldRight(t)(acc)(f))
    case Nil() => acc
  }

}
