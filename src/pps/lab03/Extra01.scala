package pps.lab03

import List._
import Person.Teacher

object Extra01 extends App {

  def courses(l: List[Person]): List[String] = map(filter(l)(_.isInstanceOf[Teacher]))(_.asInstanceOf[Teacher].course)

  def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))

  def genOp(o: (Int, Int) => Int, i: Int): Int => Int = o(_, i)

  val incBy: Int => Int => Int = x => genOp(_ + _, x)

  val minus3 = genOp(_ - _, 3)
  val div2 = genOp(_ / _, 2)
  println(compose(div2, minus3)(30)) // 13

  val plus7 = incBy(7)
  println(plus7(8)) // 15

}
