package pps.lab03

object Extra02 extends App {

  import List._

  val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
  println(foldLeft(lst)(0)(_-_)) // -16
  println(foldRight(lst)(0)(_-_)) // -8

}
