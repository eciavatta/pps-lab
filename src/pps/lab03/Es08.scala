package pps.lab03

object Es08 extends App {

  import List._

  println(max(Cons(10, Cons(25, Cons(20, Nil()))))) // Some(25)
  println(max(Nil())) // None()

}
