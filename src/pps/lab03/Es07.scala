package pps.lab03

object Es07 extends App {

  println(List.sum(List.Cons(10, List.Cons(20, List.Cons(30, List.Nil()))))) // 60

  import List._

  println(append(Cons("10", Nil()), Cons("1", Cons("2", Nil())))) // "10" ,"1" ,"2"

  println(drop(Cons(10, Cons(20, Cons(30, Nil()))), 2)) // Cons(30, Nil())
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))), 5)) // Nil()

  println(map(Cons(10, Cons(20, Nil())))(_ + 1)) // Cons(11, Cons(21, Nil()))
  println(map(Cons(10, Cons(20, Nil())))(":" + _ + ":")) // Cons(":10:", Cons(":20:",Nil()))

  println(filter(Cons(10, Cons(20, Nil())))(_ > 15)) // Cons(20, Nil())
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))(_.length <= 2)) // Cons("a", Cons("bb", Nil()))

}
